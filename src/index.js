import React from 'react';
import ReactDOM from 'react-dom';
import routes from "./router";
import { useRoutes } from "hookrouter";
import * as serviceWorker from './serviceWorker';
import {ModalContextProvider} from "./Context/ModalContext";
import ModalManager from "./components/ModalManager";

function App() {
    const routeResult = useRoutes(routes);
    return(
        <ModalContextProvider>
         <ModalManager/>
        {routeResult}
        </ModalContextProvider>
    )
}
ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
