import React, { useContext} from "react";
import { ModalContext} from "../../Context/ModalContext";
import Button from '@material-ui/core/Button';

function About () {
    const {currentModal,setCurrentModal} = useContext(ModalContext);
    const handleClickOpen = () => {
        console.log(currentModal);
        setCurrentModal('ModalOne');
    };
    return (
        <div className="About">
        <h1>About</h1>
            <Button variant="outlined" color="secondary" onClick={handleClickOpen}>
                Open dialog
            </Button>
        </div>
    )
}

export default About