import React from "react";
import Home from "./pages/Home/index";
import About from "./pages/About/index";

const routes = {
    "/" : () => <Home/>,
    "/about": ()=><About/>
};

export default routes
