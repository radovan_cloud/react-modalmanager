import React, {useContext} from "react";
import ModalOne from "./ModalOne";
import ModalTwo from "./ModalTwo";
import { ModalContext} from "../Context/ModalContext";

const Modals = {
    ModalOne,
    ModalTwo
};

const ModalManager = props =>{
    const { currentModal,setCurrentModal } = useContext(ModalContext);
    const closeModal = () => setCurrentModal(null);
    if(currentModal) {
        const ModelComponent = Modals[currentModal];
        return <ModelComponent closeModal={closeModal}/>;
    }

  return null
};

export default ModalManager